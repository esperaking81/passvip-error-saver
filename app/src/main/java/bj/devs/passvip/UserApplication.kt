package bj.devs.passvip

import android.app.Application
import bj.devs.passvip.db.UserRepository
import bj.devs.passvip.db.UserRoomDatabase
import bj.devs.passvip.network.MyRetrofit

class UserApplication : Application() {
    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    private val database by lazy { UserRoomDatabase.getDatabase(this) }
    val repository by lazy { UserRepository(database.wordDao()) }
    val retrofit by lazy { MyRetrofit.getInstance() }

}