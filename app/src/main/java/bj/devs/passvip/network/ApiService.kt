package bj.devs.passvip.network

import bj.devs.passvip.User
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
    @POST("/api/v1/utilities/passvip")
    fun sendData(@Body body: User): Call<Any>
}