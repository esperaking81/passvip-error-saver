package bj.devs.passvip

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class User(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var phone: String = "",
    var card: String = "",
    var ticket: Int = 10000,
    var image: String = ""
) {
    fun copyWith(
        phone: String? = null,
        card: String? = null,
        ticket: Int? = null,
        image: String? = null
    ): User {
        return User(
            phone = phone ?: this.phone,
            card = card ?: this.card,
            ticket = ticket ?: this.ticket,
            image = image ?: this.image
        )
    }
}