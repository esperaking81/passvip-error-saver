package bj.devs.passvip.db

import androidx.annotation.WorkerThread
import bj.devs.passvip.User
import bj.devs.passvip.dao.UserDao

class UserRepository(private val userDao: UserDao) {
    // By default Room runs suspend queries off the main thread, therefore, we don't need to
    // implement anything else to ensure we're not doing long running database work
    // off the main thread.
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(user: User) {
        userDao.insertAll(user)
    }


}