package bj.devs.passvip.db

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import bj.devs.passvip.User
import bj.devs.passvip.UserApplication
import bj.devs.passvip.network.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserViewModel(
    private val application: UserApplication,
    private val userRepository: UserRepository,
) : ViewModel() {
    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(user: User) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.insert(user)

        sendData(user)
    }

    fun clear() {
        newUser.value = User()
    }

    private fun sendData(user: User) = viewModelScope.launch(Dispatchers.IO) {
        val retrofit = application.retrofit.create(ApiService::class.java)

        retrofit.sendData(user).enqueue(object : Callback<Any> {
            override fun onResponse(p0: Call<Any>, p1: Response<Any>) {
                if (p1.isSuccessful) {
                    Log.e(TAG, "User data saved online !")
                } else {
                    Log.e(TAG, "Failed to save user data")
                }
            }

            override fun onFailure(p0: Call<Any>, p1: Throwable) {
                Log.e(TAG, "Failed to save user data !")
            }
        })
    }

    val newUser = MutableLiveData(User())

    companion object {
        private const val TAG = "UserViewModel"
    }
}

class UserViewModelFactory(
    private val application: UserApplication
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UserViewModel(application, application.repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}